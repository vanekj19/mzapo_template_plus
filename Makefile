# configuration overrides
include Makefile.conf

## various directories & files
OBJDIR     = build
SRCROOT    = src
EXECUTABLE = change_me
EXE_PATH   = $(OBJDIR)/$(EXECUTABLE)
GDB_CONF   = $(OBJDIR)/connect.gdb


################################
### Defaults + SSH config

# SSH target options
TARGET_USER ?= root
TARGET_IP   ?= 127.0.0.1
TARGET_DIR  ?= /tmp/$(whoami)
ifeq ($(TARGET_IP),127.0.0.1)
  TARGET_PORT ?= 2222
else
  TARGET_PORT ?= 22
endif

# SSH tunnelling options
ifneq ($(filter tunnel,$(MAKECMDGOALS)),)
  ifeq ($(TUNNEL_IP),)
    $(error TUNNEL_IP= not set. Please set it in Makefile.conf)
  endif
  ifeq ($(CTU_USERNAME),)
    $(error CTU_USERNAME= not set. Please set it in Makefile.conf)
  endif
  ifeq ($(CTU_USERNAME),prijmeni123)
    $(error CTU_USERNAME= not set. Please set it in Makefile.conf)
  endif
endif

# Miscellaneous SSH options
POSTEL_IP       ?= postel.felk.cvut.cz
SSH_RKEY        ?= .mzapo_key
SSH_RKEY_POSTEL ?= /opt/zynq/ssh-connect/mzapo-root-key
SSH_TARGET       = $(TARGET_USER)@$(TARGET_IP)
SSH_TGT_OPTIONS += -o Port=$(TARGET_PORT)
SSH_TGT_OPTIONS += -i $(SSH_RKEY)
SSH_TGT_OPTIONS += -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"
GDB_PORT        ?= 12345


################################
### Compiler configuration

## used c compiler
CROSS ?= arm-linux-gnueabihf-
CC     = $(CROSS)gcc
CXX    = $(CROSS)g++

## configurable flags
OPTIMIZATION ?= -O2 -s
SANITIZERS   ?=
WARNINGS     ?= -Wall -pedantic -Werror
TARGET       ?= -mcpu=cortex-a9
STATIC       ?=

## construct other flags
# depflags - automatic generation of Make dependencies between .c and .h files
# inspired by http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
DEPFLAGS = -MMD -MP -MT $@ -MF $(patsubst $(OBJDIR)/%.o,$(OBJDIR)/%.d,$@)
# cppflags - C preprocessor flags
CPPFLAGS = -I$(SRCROOT) -D_GNU_SOURCE
# cflags - C compiler options (code generation related)
CFLAGS   = -std=c11   -pthread $(OPTIMIZATION) $(SANITIZERS) $(WARNINGS) $(TARGET)
# cflags - C++ compiler options (code generation related)
CXXFLAGS = -std=c++11 -pthread $(OPTIMIZATION) $(SANITIZERS) $(WARNINGS) $(TARGET)
# ldlibs - libraries to link to
LDLIBS   = -lrt -lm


################################
### Source files

## file list
SOURCES = $(wildcard **/*.c) $(wildcard **/*.cpp)

# generate output file list
OBJFILES += $(filter %.o,$(patsubst $(SRCROOT)/%.c,$(OBJDIR)/%.o,$(SOURCES)))
OBJFILES += $(filter %.o,$(patsubst $(SRCROOT)/%.cpp,$(OBJDIR)/%.o,$(SOURCES)))
DEPFILES = $(patsubst %.o,%.d,$(OBJFILES))


################################
### Linker configuration

ifeq ($(filter %.cpp,$(SOURCES)),)
  LINK    = $(CC)
  LDFLAGS = $(CFLAGS) $(STATIC)
else
  LINK    = $(CXX)
  LDFLAGS = $(CXXFLAGS) $(STATIC)
endif


################################
### Logging control

ifeq ($(VERBOSE)$(V),)
  Q = @@
else
  Q =
endif


################################
### Build process

all: $(EXE_PATH)

# compile C source files to object files
$(OBJDIR)/%.o: $(SRCROOT)/%.c $(OBJDIR)/%.d Makefile Makefile.conf | $(OBJDIR)
	@echo " [CC] $@"
	$(Q)mkdir -p $(@D)
	$(Q)$(CC) $(CFLAGS) $(CPPFLAGS) $(DEPFLAGS) -o $@ -c $<

# compile C++ source files to object files
$(OBJDIR)/%.o: $(SRCROOT)/%.cpp $(OBJDIR)/%.d Makefile Makefile.conf | $(OBJDIR)
	@echo " [CXX] $@"
	$(Q)mkdir -p $(@D)
	$(Q)$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(DEPFLAGS) -o $@ -c $<

# link object files to final executable
$(EXE_PATH): $(OBJFILES)
	@echo " [LD] $@"
	$(Q)$(LINK) $(LDFLAGS) $^ -o $@ $(LDLIBS)

$(DEPFILES):

$(OBJDIR):
	$(Q)mkdir -p $@

-include $(DEPFILES)

clean:
	rm -rf $(OBJDIR)
	rm -f $(ZIP_NAME)

mrproper:
	rm -rf $(OBJDIR)
	rm -f Makefile.conf
	rm -f $(SSH_RKEY)

rebuild: clean all

Makefile.conf:
	$(error Makefile.conf not found. Please create it; you can start by copying Makefile.conf.example)


################################
### Remote SSH utils

tunnel:
	@echo "-- Opening SSH tunnel to kit at $(TUNNEL_IP), press Ctrl+C to exit"
	ssh -nNT $(CTU_USERNAME)@$(POSTEL_IP) -L $(TARGET_PORT):$(TUNNEL_IP):22

shell: $(SSH_RKEY)
	@echo "-- Connecting to $(TARGET_IP):$(TARGET_PORT)"
	ssh $(SSH_TGT_OPTIONS) $(SSH_TARGET)

shell-x11: $(SSH_RKEY)
	@echo "-- Connecting to $(TARGET_IP):$(TARGET_PORT) with X11 forwarding enabled"
	ssh $(SSH_TGT_OPTIONS) -X $(SSH_TARGET)

upload: $(EXE_PATH) stop-gdbserver $(SSH_RKEY)
	@echo "-- Uploading executable to $(TARGET_IP):$(TARGET_PORT)"
	ssh $(SSH_TGT_OPTIONS) $(SSH_TARGET) mkdir -p $(TARGET_DIR)
	scp $(SSH_TGT_OPTIONS) $(EXE_PATH) $(SSH_TARGET):$(TARGET_DIR)/$(EXECUTABLE)

run: upload $(SSH_RKEY)
	@echo "-- Running program on remote"
	ssh $(SSH_TGT_OPTIONS) -t $(SSH_TARGET) $(TARGET_DIR)/$(EXECUTABLE)

$(SSH_RKEY):
	@echo "-- Fetching $(SSH_RKEY) from postel"
	$(Q)scp $(CTU_USERNAME)@$(POSTEL_IP):$(SSH_RKEY_POSTEL) $@
	$(Q)chmod 0600 $@


################################
### Remote debug

run-gdbserver: upload $(TARGET_EXE) $(SSH_RKEY)
	xterm -e ssh $(SSH_TGT_OPTIONS) -L $(GDB_PORT):127.0.0.1:$(GDB_PORT) -t $(SSH_TARGET) gdbserver 127.0.0.1:$(GDB_PORT) $(TARGET_DIR)/$(EXECUTABLE) &
	sleep 2

stop-gdbserver: $(SSH_RKEY)
	ssh $(SSH_TGT_OPTIONS) $(SSH_TARGET) killall gdbserver 1>/dev/null 2>/dev/null || true

$(GDB_CONF): Makefile Makefile.conf
	echo >$(GDB_CONF) "target extended-remote 127.0.0.1:$(GDB_PORT)"
	echo >>$(GDB_CONF) "break main"
	echo >>$(GDB_CONF) "continue"

debug-ddd: run-gdbserver $(GDB_CONF)
	ddd --debugger gdb-multiarch -x $(GDB_CONF) $(EXE_PATH)

debug-gdb: run-gdbserver $(GDB_CONF)
	gdb-multiarch -x $(GDB_CONF) $(EXE_PATH)


################################
### ASan/UBSan helpers

install-san:
	@echo "-- Installing sanitizer libs on remote"
	ssh $(SSH_TGT_OPTIONS) -t $(SSH_TARGET) apt-get install -y libasan4 libubsan0


################################
### Meta

.PHONY : all clean mrproper rebuild tunnel shell shell-x11 upload run run-gdbserver stop-gdbserver debug-ddd debug-gdb install-san zip
